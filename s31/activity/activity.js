// 1. What directrequire("http");ive is used by Node.js in loading the modules it needs?

// require
// Ex. require("http");

// 2. What Node.js module contains a method for server creation?

// http
// let http = require("http");

// 3. What is the method of the http object responsible for creating a server using Node.js?

// createServer
// Ex. variable.createServer(function (request, response)

// 4. What method of the response object allows us to set status codes and content types?
	
// writeHead
// Ex. response.writeHead(200, {'Content-Type':'text/plain'});


// 5. Where will console.log() output its contents when run in Node.js?

// In the terminal (Gitbash)

// 6. What property of the request object contains the address's endpoint?

// request.url

let http = require("http");

const port = 3000;

const app = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end('Welcome to the login page.');
	}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
});

app.listen(port);

console.log(`Server is now successfully running at localhost:${port}.`);