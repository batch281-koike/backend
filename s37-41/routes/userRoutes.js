const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exist in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// My answer Activity s38 for retrieving the details of the user
/*router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})*/

// S38 ACTIVTY Answer (router.post was changed to get due to s39 discussion)

// Route for retrieving user details
// The "auth.verify" acts as middleware to ensure that user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// S41 Discussion Route to enroll user to a course
/*router.post("/enroll", (req, res) => {

	let data = {
		
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})*/


// S41 My Activity Answer
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		courseId : req.body.courseId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});


module.exports = router;