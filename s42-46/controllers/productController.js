const User = require("../models/User");
const Product = require("../models/Product");
// const Order = require("../models/Order");

// Create Product (Admin Only)
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		image : reqBody.image
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		} else {
			return product;
		}
	})
}

// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};


// Retrieving a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

// Update a product information
module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		image : reqBody.image
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
		if(error){
			// return "Failed to update the product.";
			return false;
		} else {
			return product;
		}
	})
}


// Archiving a product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let archiveProduct = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return product;
		}
	})
}

// Activating a product
module.exports.activateProduct = (reqParams, reqBody) => {

	let activateProduct = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return product;
		}
	})
}