const User = require("../models/User");
const Product = require("../models/Product");
// const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
/*	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false;

		} else {
			return true;
		}
	})
}*/
		// If the email already exists in the database S50-55 activity edit for Register.js
		 return User.findOne({ email: reqBody.email }).then((existingUser) => {
		   if (existingUser) {
		      // Email already exists
		      return false;
		   } else {
		      // Create a new user
		     let newUser = new User({
		        firstName: reqBody.firstName,
		        lastName: reqBody.lastName,
		        email: reqBody.email,
		        mobileNo: reqBody.mobileNo,
		        password: bcrypt.hashSync(reqBody.password, 10),
		     });

		     return newUser.save().then((user, error) => {
		       if (error) {
		          // User registration failed
		          return false;
		       } else {
		          // User registration successful
		          return true;
		       }
		     });
		   }
		 });
	};

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}

			} else {
				return false;
			}
		}
	})
}

// non-admin user checkout (Create Order)
/*module.exports.createOrder = (reqBody) => {

	let newOrder = new Order({
		userId: reqBody.userId,
		productId: reqBody.productId,
		quantity: reqBody.quantity,
		totalAmount: reqBody.totalAmount
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return "Failed to create a order.";
		} else {
			return "Successfully created a order.";
		}
	})
}*/

module.exports.order = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
			user.products.push({
				productId : data.productId,
				productName : data.productName,
				quantity: data.quantity,
			});

			return user.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userOrders.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	};
}


// Retrieving user details
module.exports.retrieveProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Set User As Admin (Admin Only)
module.exports.setUserAsAdmin = (reqBody) => {

	let setUserAsAdmin = {
		userId: reqBody,
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqBody.userId, setUserAsAdmin).then((user, error) => {
		if(error){
			return "Failed to set user as Admin.";
		} else {
			return "Successfully set user as Admin.";
		}
	})
}

// Retrieving authenticated user's order
module.exports.getOrders = (data) => {
	return User.findById(data.userId).then(result => {
		return result.products;
	});
};

// Retrieving all orders (Admin Only)
module.exports.getAllOrders = () => {
	return User.find().then(result => {
		return result;
	});
};