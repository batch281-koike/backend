const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for non-admin user checkout (Create Order)
/*router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == false) {
		userController.createOrder(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Orders can only be accepted from users.");
	}
})*/
router.post("/checkout", auth.verify, (req, res) => {

	let data = {
		userId: req.body.userId,
		productId : req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == false){
	userController.order(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Orders can only be accepted from users.");
	}
});

// router.post('/checkout', auth.verify, (req, res) => {
//   const data = {
//     userId: req.body.userId,
//     productId: req.body.productId,
//     productName: req.body.productName,
//     quantity: req.body.quantity,
//     isAdmin: auth.decode(req.headers.authorization).isAdmin,
//   };

//   if (data.isAdmin === false) {
//     userController.order(data)
//       .then((resultFromController) => {
//         if (resultFromController === true) {
//           res.send(true); // Send a boolean response indicating success
//         } else {
//           res.send(false); // Send a boolean response indicating failure
//         }
//       })
//       .catch((error) => {
//         console.log(error);
//         res.send(false);
//       });
//   } else {
//     res.send(false);
//     console.log('Orders can only be accepted from users.');
//   }
// });

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.retrieveProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for Set User As Admin (Admin Only)
router.patch("/setAsAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		userController.setUserAsAdmin(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

// Route for retrieving authenticated user's order
router.get("/myOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all orders (Admin Only)
router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

module.exports = router;