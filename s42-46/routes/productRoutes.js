const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Create Product (Admin Only)
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product information
router.put("/:productId/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

// Route archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

// Route for activating a product
router.patch("/:productId/activate", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
		productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
		console.log("Admin status is required.");
	}
})

module.exports = router;