const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

         email : {
            type : String,
            required : [true, "Email is required"]
        },
        password : {
            type : String,
            required : [true, "Password is required"]
        },
        isAdmin : {
            type : Boolean,
            default : false
        },
        products : [
            {
                productId : {
                    type : String,
                    required : [true, "Product ID is required"]
                },
                productName : {
                    type : String,
                    required : [true, "Product name is required"]
                },
                quantity : {
                    type : Number,
                    default : new Number()
                }
            }
        ],

        totalAmount : {
            type : Number,
            default : new Number()
        },
        
        purchasedOn : {
            type : Date,
            default : new Date()
        }

})

module.exports = mongoose.model("User", userSchema);
