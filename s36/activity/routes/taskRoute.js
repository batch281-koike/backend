const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskController");


// Activity

// Route to get specific task
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

// Router to update a specific task
router.put("/:id/complete", (req, res) => {
	taskController.updateSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})