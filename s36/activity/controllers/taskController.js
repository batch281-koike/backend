const Task = require("../models/task");

// Activity 

// Specific task
module.exports.getSpecificTask = (taskId, newStatus) => {

	return Task.findById(taskId).then(result => {
		result.status = "complete";
		return result;
	})
}

// update a specific task
module.exports.updateSpecificTask = (taskId) => {

	return Task.findById(taskId).then((result, error) => {

		if(error){
			console.log(error);
			return false;
		}

		result.status = "complete";

		return result.save().then((updateTask, saveErr) => { 

			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}