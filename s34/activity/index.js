const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// HOME
app.get("/home", (req, res) =>{
	res.send("Welcome to the home page");
});

// USER
let users = [];

app.get("/users", (req, res) =>{
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(users);
	} 
});

// DELETE
app.delete("/delete-user", (req, res) => {
    let { username } = req.body;

    let message;

    if (users.length > 0) {
        for (let i = 0; i < users.length; i++) {
            if (req.body.username === users[i].username) {
                users.splice(i, 1);
                message = `User ${req.body.username} has been deleted.`;
                console.log(users);
                break;
            }
        }
        if (!message) {
        message = "User does not exist."
        }
    } else {
        message = "No users found."
    }

    res.send(message);
});

// Listen
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at ${port}`))
}

module.exports = app;