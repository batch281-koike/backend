// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let s = 0; s < string.length; s++){
	if(
		string[s] == "a" ||
		string[s] == "e" ||
		string[s] == "i" ||
		string[s] == "o" ||
		string[s] == "u"
		){
		continue;
	}
	else {
		filteredString = filteredString + string[s];;
	}
}

console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}
