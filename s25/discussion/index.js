// JSON Object
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON as objects
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays
/*"cities": [
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"}
]
*/
// Mini Activity - Create a Jason Array that will hold atleast three breeds of dogs with properties: name, age, breed

/*"dogs": [
	{"name": "Popi", "age": "4", "breed": "Shih Tzu"},
	{"name": "Jean", "age": "3", "breed": "Pug"},
	{"name": "Popo", "age": "5", "breed": "Bulldog"}
]*/

// JSON Methods

// Convert Data into Stringified JSON - it is a JS object converted into JSON data.

let batcherArr = [{ batchName: 'Batch X'}, { batchName: 'Batch Y'}];

// the "stringify" method is used to convert JavaScript objects into a string

console.log('Result from stringify method:');
console.log(JSON.stringify(batcherArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

// Using Stringify method with variables
// User details 
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);*/

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log("Result from parse Method")
console.log(JSON.parse(batchesJSON));