// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
    let Friends = {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    }

    let myPokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
    
    function Trainer(name, age, pokemon, friends){
// Properties  
        this.name = name;
        this.age = age;
        this.pokemon = pokemon;
        this.friends = friends;
// Methods
        this.talk = function(){
        console.log(this.pokemon[0] + "! I choose you!");
        }
    }

// Initialize/add the given object properties and methods
 
    let trainer = new Trainer('Ash Ketchum', 10, myPokemon, Friends);


 // Check if all properties and methods were properly added   
    console.log(trainer);

// Access object properties using dot notation
    console.log('Result from dot notation:');
    console.log(trainer.name);

// Access object properties using square bracket notation
    console.log('Result from square bracket notation:');
    console.log(trainer['pokemon']);

// Access the trainer "talk" method
    console.log("Result of talk method:")
    trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
    function Pokemon(name, level, health, attack){
        this.name = name;
        this.level = level;
        this.health = health;
        this.attack = attack;

        this.tackle = function(target, attacker){
        this.targetPokemonsHealth = target.health - attacker.attack;
        target.health = this.targetPokemonsHealth;
            console.log(this.name + ' tackled ' + target.name);
            console.log(target.name + " health is now reduced to " + this.targetPokemonsHealth);
            if(this.targetPokemonsHealth <= 0){
                target.faint();
            }
            console.log(target);
        }
        this.faint = function(){
            console.log(this.name + ' fainted.');
        }
    }

// Create/instantiate a new pokemon
    let pikachu = new Pokemon('Pikachu', 12, 24, 12);
    console.log(pikachu);
// Create/instantiate a new pokemon
    let geodude = new Pokemon('Geodude', 8, 16, 8);
    console.log(geodude);
// Create/instantiate a new pokemon
    let mewtwo = new Pokemon('Mewtwo', 100, 200, 100);
    console.log(mewtwo);
// Invoke the tackle method and target a different object
    geodude.tackle(pikachu, geodude);

// Invoke the tackle method and target a different object
    mewtwo.tackle(geodude, mewtwo);





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}